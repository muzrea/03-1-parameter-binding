package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import java.io.IOException;
import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UsersControllerTest {
    @Autowired
    MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void should_serialize_successfully() throws IOException {
        String json = "{\"name\" : \"kamil\"}";
        Contract contract = objectMapper.readValue(json, Contract.class);
        assertEquals(contract.getName(), "kamil");
    }

    @Test
    void should_serialize_successfully_without_setter() throws IOException {
        String json = "{\"name\" : \"bob\"}";
        ContractNoSetter contractNoSetter = objectMapper.readValue(json, ContractNoSetter.class);
        assertEquals(contractNoSetter.getName(), "bob");
    }

    @Test
    void should_deserialize_successfully_with_setter() throws IOException {
        Contract contract = new Contract();
        contract.setName("Susan");
        String carAsString = objectMapper.writeValueAsString(contract);
        assertEquals(carAsString, "{\"name\":\"Susan\"}");
    }

    @Test
    void should_response_success() throws Exception {
        mockMvc.perform(get("/api/users/2"))
                .andExpect(content().string("userId is 2"))
                .andExpect(status().isOk());
    }

    @Test
    void should_response_failed_use_int_variable_type() throws Exception {
        mockMvc.perform(get("/api/teachers/2"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_userId_bookId() throws Exception {
        mockMvc.perform(get("/api/users/2/books/4"))
                .andExpect(status().isOk())
                .andExpect(content().string("userId is 2 + bookId is 4"));
    }

    @Test
    void should_return_name_value_use_param() throws Exception {
        mockMvc.perform(get("/api/teacherInfo").param("name", "Molly"))
                .andExpect(content().string("teacher's name is Molly"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_default_value() throws Exception {
        mockMvc.perform(get("/api/teacher"))
                .andExpect(content().string("teacher's gender is female"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_collection_when_delivery() throws Exception {
        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        mockMvc.perform(get("/api/teacher/Info").param("numbers", "1,2,3"))
                .andExpect(status().isOk());
    }

    @Test
    void should_serialize_requestBody_successfully() throws Exception {
        mockMvc.perform(post("/api/datetimes")
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string( "\"2019-10-01T10:00:00Z\""));
    }

    @Test
    void should_serialize_requestBody_failed_not_use_iso() throws Exception {
        mockMvc.perform(post("/api/datetimes")
                .content("{ \"dateTime\": \"2019-10-01\" }")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(400));
    }


    @Test
    void should_return_400_when_use_notNull() throws Exception {
        mockMvc.perform(post("/api/student/name")
                .content("{\"gender\":\"male\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_successful_when_use_notNull() throws Exception {
        mockMvc.perform(post("/api/student/name")
                .content("{\"name\":\"Mack\",\"gender\":\"male\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("The student name is Mack,The student gender is male"));
    }

    @Test
    void should_return_400_when_use_size() throws Exception {
        mockMvc.perform(post("/api/student/name")
                .content("{\"name\":\"a\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_size_out_of_range() throws Exception {
        mockMvc.perform(post("/api/student/name")
                .content("{\"name\":\"kamil\",\"yearOfBirth\":99}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_email_format_error() throws Exception {
        mockMvc.perform(post("/api/student/name")
                .content("{\"name\":\"kamil\",\"yearOfBirth\":1300,\"email\":\"ss@s.s\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_successful_when_email_format_correct() throws Exception {
        mockMvc.perform(post("/api/student/name")
                .content("{\"name\":\"kamil\",\"gender\":\"male\",\"yearOfBirth\":1300,\"email\":\"ss@s.s\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("The student name is kamil,The student gender is male"));
    }
}