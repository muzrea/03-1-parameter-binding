package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.*;

@RestController
public class Student {
    @NotNull
    @Size(min = 4, max = 16)
    private String name;
    private String gender;
    private Integer age;
    @Min(1000)
    @Max(9999)
    private Integer yearOfBirth;
    @Email(regexp = "\\w+@\\w+.\\w+")
    private String email;

    public void Student() {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(Integer yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
