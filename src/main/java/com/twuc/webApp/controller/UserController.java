package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {
    @GetMapping("/users/{userId}")
    public String getUserId(@PathVariable Integer userId) {
        return "userId is " + userId;
    }

    @GetMapping("/teachers/teacherId}")
    public String getTeacherId(@PathVariable int teacherId) {
        return "teacherId is " + teacherId;
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public String getTeacherId(@PathVariable String userId, @PathVariable String bookId) {
        return "userId is " + userId + " + bookId is " + bookId;
    }

    @GetMapping("/teacherInfo")
    public String getTeacherName(@RequestParam String name) {
        return "teacher's name is " + name;
    }

    @GetMapping("/teacher")
    public String getDefaultGender(@RequestParam(defaultValue = "female") String gender) {
        return "teacher's gender is " + gender;
    }

    @GetMapping("/teacher/Info")
    public List<Integer> getCollection(@RequestParam(value = "numbers") List<Integer> numbers) {
        return numbers;
    }

    @PostMapping("/student")
    public String createStudent(@RequestParam Student student) {
        return student.getName();
    }

    @PostMapping("/datetimes")
    public ZonedDateTime createStudent(@RequestBody DateTime time) {
        return time.getDateTime();
    }

    @PostMapping("/student/name")
    public String getStudent(@RequestBody @Valid Student student) {
        return "The student name is " + student.getName() + ",The student gender is " +student.getGender();
    }
}
